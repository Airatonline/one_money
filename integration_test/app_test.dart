import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:one_money/main.dart' as app;
import 'package:one_money/presenter/widgets/category_icon.dart';

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  testWidgets("testing additional spent in db", (WidgetTester tester) async {
    app.main();
    await tester.pumpAndSettle();
    
    expect(find.byWidgetPredicate((widget) => widget is CategoryIcon), findsWidgets);
    // Finds the floating action button to tap on.
    final fab = find.text('Подарки');
    // Emulate a tap on the floating action button.
    await tester.tap(fab);
    // Trigger a frame.
    await tester.pumpAndSettle();
    expect(find.byWidgetPredicate((widget) => widget is TextFormField), findsOneWidget);
    final textField = find.byWidgetPredicate((widget) => widget is TextFormField);
    await tester.enterText(textField, '1000');
    final okBtn = find.text('Ок');
    await tester.tap(okBtn);
    await tester.pumpAndSettle();
    expect(find.text('1000₽'), findsWidgets);
  });
}