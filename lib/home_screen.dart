import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:one_money/presenter/cards/cards_cubit.dart';
import 'package:one_money/presenter/category/category_cubit.dart';
import 'package:one_money/presenter/category/category_screen.dart';
import 'package:one_money/presenter/cards/cards_screen.dart';

import 'presenter/overview/overview_screen.dart';
import 'package:rxdart/rxdart.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HomeScreen();
}

class _HomeScreen extends State<HomeScreen> {
  final PageController controller = PageController(initialPage: 1);
  final StreamController<int> pageStream = BehaviorSubject();

  @override
  void initState() {
    super.initState();
    pageStream.stream.listen((newIndex) {
      controller.animateToPage(newIndex,
          duration: const Duration(milliseconds: 300),
          curve: Curves.decelerate);
    });
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<CategoryCubit>(
          create: (_) {
            final bloc = CategoryCubit()..init();
            return bloc;
          },
        ),
        BlocProvider<CardsCubit>(
          create: (_) {
            final bloc = CardsCubit()..init();
            return bloc;
          },
        )
      ],
      child: Scaffold(
        bottomNavigationBar: StreamBuilder<int>(
          stream: pageStream.stream,
          initialData: 1,
          builder: (context, snapshot) {
            return BottomNavigationBar(
              currentIndex: snapshot.data!,
              onTap: (newIndex) {
                pageStream.add(newIndex);
              },
              items: const [
                BottomNavigationBarItem(
                    icon: Icon(Icons.file_copy), label: 'Счета'),
                BottomNavigationBarItem(
                    icon: Icon(Icons.circle), label: 'Траты'),
                BottomNavigationBarItem(
                    icon: Icon(Icons.circle), label: 'Обзор'),
              ],
            );
          },
        ),
        body: PageView(
          children: [CardsScreen(), CategoryScreen(), OverviewScreen()],
          controller: controller,
          physics: NeverScrollableScrollPhysics(),
        ),
      ),
    );
  }
}
