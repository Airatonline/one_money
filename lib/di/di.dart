import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:hive/hive.dart';
import 'package:one_money/data/models/category_model.dart';
import 'package:one_money/data/models/money_model.dart';
import 'package:one_money/data/models/spend_element_model.dart';
import 'package:one_money/data/repository/impl/repository_impl.dart';
import 'package:one_money/data/repository/interface/repository.dart';
import 'package:one_money/data/services/db/impl/database_impl.dart';
import 'package:one_money/data/services/db/inteface/database.dart';
import 'package:path_provider/path_provider.dart';

class DI {
  Future<Injector> initialize(Injector injector) async {
    var path = await getApplicationSupportDirectory();
    Hive
      ..init(path.path)
      ..registerAdapter(CategoryModelAdapter())
      ..registerAdapter(MoneyModelAdapter())
      ..registerAdapter(SpendElementModelAdapter());
    final spendBox = await Hive.openBox<SpendElementModel>('spend');
    final categoryBox = await Hive.openBox<CategoryModel>('category');
    final moneyBox = await Hive.openBox<MoneyModel>('money');
    final database = DatabaseImpl(spendBox, categoryBox, moneyBox);
    injector.map<Database>((injector) => database);
    injector.map<Repository>((injector) => RepositoryImpl(database));
    return injector;
  }
}
