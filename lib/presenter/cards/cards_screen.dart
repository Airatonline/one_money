import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:one_money/presenter/cards/cards_cubit.dart';
import 'package:one_money/presenter/cards/widgets/input_card_value_widget.dart';
import 'package:one_money/presenter/category/category_cubit.dart';
import 'package:one_money/presenter/widgets/add_bill_sheet_widget.dart';
import 'package:one_money/presenter/widgets/category_icon.dart';

class CardsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CardsCubit, CardsState>(builder: (context, state) {
      final cubit = CardsCubit.of(context);
      return Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 100,
                child: AppBar(
                  centerTitle: true,
                  title: Text('Все счета'),
                  bottom: PreferredSize(
                    preferredSize: Size(double.infinity, 40),
                    child: Text(
                      state.allSpendMoney.toStringAsFixed(2),
                      style: Theme.of(context)
                          .appBarTheme
                          .titleTextStyle!
                          .copyWith(fontSize: 20),
                    ),
                  ),
                ),
              ),
              Text(
                'Счета',
                style: Theme.of(context)
                    .textTheme
                    .headline5!
                    .copyWith(color: Colors.black45),
                textAlign: TextAlign.start,
              ),
              const SizedBox(
                height: 16,
              ),
              Expanded(
                  child: ListView(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                children: state.allSavedMoney
                    .map((e) => InkWell(
                          onTap: () async {
                            final value = await showModalBottomSheet<int>(
                              isScrollControlled: true,
                              context: context,
                              builder: (context) =>
                                  InputCardValueWidget(state.allSavedMoney),
                            );
                            if (value != null) {
                              cubit.saveMoney(
                                  e.copyWith(value: value.toDouble()));
                              CategoryCubit.of(context).init();
                            }
                          },
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8),
                            child: Row(
                              children: [
                                CategoryIcon(
                                    categoryColor: Colors.green,
                                    categoryIcon: Icons.account_balance_wallet),
                                const SizedBox(
                                  width: 16,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(e.name),
                                    Text(
                                      e.value.toStringAsFixed(2),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ))
                    .toList(),
              ))
            ],
          ),
          Positioned.fill(
              bottom: 16,
              right: 16,
              child: Align(
                alignment: Alignment.bottomRight,
                child: FloatingActionButton(
                  onPressed: () async {
                    final value = await showModalBottomSheet<String>(
                      isScrollControlled: true,
                      context: context,
                      builder: (context) => AddBillSheetWidget('Название счета',
                          state.allSavedMoney.map((e) => e.name).toList()),
                    );
                    if (value != null) {
                      cubit.addBill(value);
                    }
                  },
                  child: Icon(Icons.add),
                ),
              ))
        ],
      );
    });
  }
}
