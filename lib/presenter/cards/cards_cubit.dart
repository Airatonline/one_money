import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:one_money/data/models/money_model.dart';
import 'package:one_money/data/repository/interface/repository.dart';

class CardsCubit extends Cubit<CardsState> {
  CardsCubit() : super(CardsState());

  final repository = Injector().get<Repository>();

  static CardsCubit of(BuildContext context) =>
      BlocProvider.of<CardsCubit>(context);

  Future<void> init() async {
    final allSavedMoney = await repository.getAllMoney();
    emit(
      CardsState(
        allSavedMoney: allSavedMoney,
        allSpendMoney: await repository.getBalance(),
      ),
    );
  }

  void saveMoney(MoneyModel value) {
    repository.saveMoney(value);
    init();
  }

  void addBill(String name) {
    repository.addBill(name);
    init();
  }
}

class CardsState {
  final List<MoneyModel> allSavedMoney;
  final double allSpendMoney;

  CardsState({
    this.allSavedMoney = const [],
    this.allSpendMoney = 0,
  });
}
