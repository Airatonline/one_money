import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:one_money/data/models/money_model.dart';

class InputCardValueWidget extends StatefulWidget {
  final List<MoneyModel> billList;

  const InputCardValueWidget(
    this.billList,
  );

  @override
  State<StatefulWidget> createState() => _InputCardValueWidgetState();
}

class _InputCardValueWidgetState extends State<InputCardValueWidget> {
  final textController = TextEditingController();
  late MoneyModel selectedBill;

  @override
  void dispose() {
    textController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    selectedBill = widget.billList.first;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: SafeArea(
        minimum: const EdgeInsets.all(16),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  onTap: sendResult,
                  child: Text(
                    'Ок',
                    style: TextStyle(color: Colors.black, fontSize: 16),
                  ),
                ),
                InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    'Отмена',
                    style: TextStyle(color: Colors.black, fontSize: 16),
                  ),
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                DropdownButton<MoneyModel>(
                  items: widget.billList
                      .map(
                        (e) => DropdownMenuItem<MoneyModel>(
                          value: e,
                          child: Text(
                            e.name,
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1
                                ?.copyWith(color: Colors.black),
                          ),
                          onTap: () {
                            selectedBill = e;
                          },
                        ),
                      )
                      .toList(),
                  value: selectedBill,
                  onChanged: (e) {
                    setState(() {
                      selectedBill = e!;
                    });
                  },
                ),
                Text(
                  'Добавление денег на счет',
                  style: TextStyle(color: Colors.black),
                ),
              ],
            ),
            TextFormField(
              controller: textController,
              keyboardType: TextInputType.number,
              style: TextStyle(color: Colors.black),
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              onEditingComplete: sendResult,
            ),
            InputKeyboard(
              addValue: (value) {
                if (textController.text.isEmpty && value == 0) {
                  return;
                }
                textController.text = textController.text + value.toString();
              },
              removeValue: () {
                if (textController.text.isNotEmpty) {
                  textController.text = textController.text
                      .substring(0, textController.text.length - 1);
                }
              },
            )
          ],
        ),
      ),
    );
  }

  void sendResult() {
    Navigator.of(context).pop(int.tryParse(textController.text) ?? 0);
  }
}

class InputKeyboard extends StatelessWidget {
  const InputKeyboard(
      {required this.addValue, required this.removeValue, Key? key})
      : super(key: key);

  final Function(int) addValue;
  final VoidCallback removeValue;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: KeyKeyboard(
                        value: Text('7', textAlign: TextAlign.center),
                        callback: () => addValue.call(7)),
                  ),
                  Expanded(
                    child: KeyKeyboard(
                        value: Text('8', textAlign: TextAlign.center),
                        callback: () => addValue.call(8)),
                  ),
                  Expanded(
                    child: KeyKeyboard(
                        value: Text('9', textAlign: TextAlign.center),
                        callback: () => addValue.call(9)),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: KeyKeyboard(
                        value: Text('4', textAlign: TextAlign.center),
                        callback: () => addValue.call(4)),
                  ),
                  Expanded(
                    child: KeyKeyboard(
                        value: Text('5', textAlign: TextAlign.center),
                        callback: () => addValue.call(5)),
                  ),
                  Expanded(
                    child: KeyKeyboard(
                        value: Text('6', textAlign: TextAlign.center),
                        callback: () => addValue.call(6)),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: KeyKeyboard(
                        value: Text('1', textAlign: TextAlign.center),
                        callback: () => addValue.call(1)),
                  ),
                  Expanded(
                    child: KeyKeyboard(
                        value: Text('2', textAlign: TextAlign.center),
                        callback: () => addValue.call(2)),
                  ),
                  Expanded(
                    child: KeyKeyboard(
                        value: Text('3', textAlign: TextAlign.center),
                        callback: () => addValue.call(3)),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: KeyKeyboard(
                        value: Text(
                          '0',
                          textAlign: TextAlign.center,
                        ),
                        callback: () => addValue.call(0)),
                  )
                ],
              )
            ],
          ),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            KeyKeyboard(
                value: Icon(
                  Icons.backspace_outlined,
                  color: Colors.grey,
                ),
                callback: removeValue.call)
          ],
        ),
      ],
    );
  }
}

class KeyKeyboard extends StatelessWidget {
  const KeyKeyboard({
    required this.value,
    required this.callback,
    Key? key,
  }) : super(key: key);
  final Widget value;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: callback,
      child: Container(
          padding: const EdgeInsets.all(16),
          decoration: BoxDecoration(border: Border.all(color: Colors.grey)),
          child: value),
    );
  }
}
