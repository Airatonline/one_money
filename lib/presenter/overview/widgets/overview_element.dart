import 'package:flutter/material.dart';
import 'package:one_money/data/models/category_model.dart';
import 'package:one_money/presenter/helpers/icon_helper.dart';
import 'package:one_money/presenter/widgets/category_icon.dart';

class OverviewElement extends StatelessWidget {
  const OverviewElement(
      {required this.category,
      required this.value,
      required this.monthCost,
      required this.maxCategoryCost});

  final CategoryModel category;
  final double value;
  final double monthCost;
  final double maxCategoryCost;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Row(
        children: [
          CategoryIcon(
            categoryColor: Color(category.color),
            categoryIcon: IconHelper.getIconData(category.icon),
          ),
          SizedBox(
            width: 16,
          ),
          Expanded(
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      category.name,
                      style: Theme.of(context).textTheme.headline1,
                    ),
                    Text('${value.toStringAsFixed(0)}₽')
                    // Text('${(value).toStringAsFixed(0)}%')
                  ],
                ),
                SizedBox(
                  width: double.infinity,
                  child: Row(
                    children: [
                      Flexible(
                          flex: (value / maxCategoryCost * 100).round(),
                          child: Container(
                            height: 5,
                            color: Color(category.color),
                          )),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 8,
                        ),
                        child: Text(
                            '${(value / monthCost * 100).toStringAsFixed(0)}%'),
                      ),
                      Flexible(
                          flex: 100 - (value / maxCategoryCost * 100).round(),
                          child: Container(
                            height: 5,
                            color: Colors.black12,
                          ))
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
