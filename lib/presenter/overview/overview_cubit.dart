import 'dart:collection';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:one_money/data/models/category_model.dart';
import 'package:one_money/data/repository/interface/repository.dart';

class OverViewCubit extends Cubit<OverViewState> {
  OverViewCubit() : super(OverViewState());

  final repository = Injector().get<Repository>();

  //TODO: refact this:)
  Future<void> init() async {
    emit(OverViewState(isLoading: true));
    //Получение товаров за месяц
    final spendGoods = await repository.getSpendGoods(
      DateTime(DateTime.now().year, DateTime.now().month),
      DateTime(DateTime.now().year, DateTime.now().month + 1),
    );
    // Получение трат за месяц
    double allCost = 0;
    spendGoods.forEach((element) {
      allCost += element.cost;
    });
    // Получение трат за сегодняшний день
    final spendGoodsToday = await repository.getSpendGoods(
        DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day),
        DateTime(DateTime.now().year, DateTime.now().month + 1,
            DateTime.now().day + 1));
    double todayCost = 0;
    spendGoodsToday.forEach((element) {
      todayCost += element.cost;
    });
    // Получение трат за неделю
    final spendCurWeek = await repository.getSpendGoods(
        DateTime(DateTime.now().year, DateTime.now().month,
            DateTime.now().day - DateTime.now().weekday),
        DateTime(DateTime.now().year, DateTime.now().month + 1,
            DateTime.now().day + 1));
    double weekCost = 0;
    spendCurWeek.forEach((element) {
      weekCost += element.cost;
    });
    // Получение списка категорий с тратами в процентном выражение
    final categories = await repository.getCategories();
    // Получение списка категорий с тратами
    final spendMap = <CategoryModel, double>{};
    spendGoods.forEach((element) {
      final category = categories
          .singleWhere((category) => category.id == element.categoryId);
      spendMap[category] = (spendMap[category] ?? 0) + element.cost;
    });

    // Сортировка Мар
    var sortedKeys = spendMap.keys.toList(growable: false)
      ..sort((k2, k1) => spendMap[k1]!.compareTo(spendMap[k2]!));
    LinkedHashMap<CategoryModel, double> sortedMap =
        new LinkedHashMap.fromIterable(sortedKeys,
            key: (k) => k, value: (k) => spendMap[k]!);
    emit(
      OverViewState(
          avgDayCost: allCost / DateTime.now().day,
          todayCost: todayCost,
          currentWeekCost: weekCost,
          monthCost: allCost,
          spendList: sortedMap),
    );
  }
}

class OverViewState {
  final double avgDayCost;
  final double todayCost;
  final double currentWeekCost;
  final double monthCost;
  final bool isLoading;
  final Map<CategoryModel, double> spendList;

  OverViewState(
      {this.avgDayCost = 0,
      this.todayCost = 0,
      this.currentWeekCost = 0,
      this.monthCost = 0,
      this.spendList = const {},
      this.isLoading = false});
}
