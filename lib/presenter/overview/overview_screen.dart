import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:one_money/presenter/category/category_cubit.dart';
import 'package:one_money/presenter/overview/overview_cubit.dart';
import 'package:one_money/presenter/overview/widgets/overview_element.dart';

class OverviewScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      child: BlocBuilder<OverViewCubit, OverViewState>(
        bloc: OverViewCubit()..init(),
        builder: (context, state) {
          if (state.isLoading == true) {
            return const CircularProgressIndicator();
          }
          return Column(
            children: [
              SizedBox(
                height: 140,
                child: AppBar(
                  centerTitle: true,
                  title: _buildInvoiceTitle(context),
                  // actions: [
                  //   IconButton(
                  //     onPressed: () {},
                  //     icon: Icon(Icons.edit),
                  //   )
                  // ],
                  bottom: _buildBottomTabBar(context),
                ),
              ),
              _buildStatistic(context, state),
              _buildSpendList(context, state),
            ],
          );
        },
      ),
    );
  }

  Widget _buildStatistic(BuildContext context, OverViewState state) {
    return Container(
      padding: const EdgeInsets.fromLTRB(16, 8, 16, 0),
      decoration:
          BoxDecoration(color: Theme.of(context).bannerTheme.backgroundColor!),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              Text('День в среднем'),
              Text(state.avgDayCost.toStringAsFixed(2).toString())
            ],
          ),
          Column(
            children: [
              Text('Сегодня'),
              Text(
                state.todayCost.toString(),
              ),
            ],
          ),
          Column(
            children: [
              Text('Неделю'),
              Text(
                state.currentWeekCost.toString(),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _buildSpendList(BuildContext context, OverViewState state) {
    return ListView(
      shrinkWrap: true,
      children: state.spendList.entries.map((category) {
        return Row(
          children: [
            Expanded(
                child: OverviewElement(
              category: category.key,
              value: category.value,
              monthCost: state.monthCost,
              maxCategoryCost: state.spendList.values.first,
            ))
          ],
        );
      }).toList(),
    );
  }

  PreferredSize _buildBottomTabBar(BuildContext context) {
    final cubit = CategoryCubit.of(context);
    return PreferredSize(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            IconButton(
                onPressed: () {
                  cubit.changeMonth(DateTime(cubit.state.startTime.year,
                      cubit.state.startTime.month - 1));
                },
                icon: Icon(Icons.chevron_left)),
            Text(
              '${DateFormat('MMMM').format(cubit.state.startTime)}',
              style: Theme.of(context)
                  .appBarTheme
                  .titleTextStyle!
                  .copyWith(fontSize: 14),
            ),
            IconButton(
                onPressed: () {
                  cubit.changeMonth(DateTime(cubit.state.startTime.year,
                      cubit.state.startTime.month + 1));
                },
                icon: Icon(Icons.chevron_right)),
          ],
        ),
        preferredSize: Size(double.infinity, 40));
  }

  Widget _buildInvoiceTitle(BuildContext context) {
    final cubit = CategoryCubit.of(context);
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(
          'Все счета',
          style: Theme.of(context)
              .appBarTheme
              .titleTextStyle!
              .copyWith(fontSize: 12),
        ),
        SizedBox(
          height: 4,
        ),
        Text(
          '${cubit.state.allCost}₽',
          style: Theme.of(context)
              .appBarTheme
              .titleTextStyle!
              .copyWith(fontSize: 20),
        ),
      ],
    );
  }
}
