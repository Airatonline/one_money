
import 'package:flutter/material.dart';
import 'package:one_money/data/models/money_model.dart';
import 'package:one_money/domain/entity/category_entity.dart';
import 'package:one_money/domain/entity/spend_entity.dart';

class InputValueSheetWidget extends StatefulWidget {
  final CategoryEntity category;
  final List<MoneyModel> billList;

  InputValueSheetWidget(this.category,
      this.billList,);

  @override
  State<StatefulWidget> createState() => _InputValueSheetState();
}

class _InputValueSheetState extends State<InputValueSheetWidget> {
  final ValueNotifier<String> textController = ValueNotifier('');
  late MoneyModel selectedBill;

  @override
  void dispose() {
    textController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    selectedBill = widget.billList.first;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      minimum: const EdgeInsets.all(16),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              DropdownButton<MoneyModel>(
                items: widget.billList
                    .map(
                      (e) =>
                      DropdownMenuItem<MoneyModel>(
                        value: e,
                        child: Text(
                          e.name,
                          style: Theme
                              .of(context)
                              .textTheme
                              .bodyText1
                              ?.copyWith(color: Colors.black),
                        ),
                        onTap: () {
                          selectedBill = e;
                        },
                      ),
                )
                    .toList(),
                value: selectedBill,
                onChanged: (e) {
                  setState(() {
                    selectedBill = e!;
                  });
                },
              ),
              InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  'Отмена',
                  style: TextStyle(color: Colors.black, fontSize: 16),
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                'Категория: ${widget.category.name}',
                style: TextStyle(color: Colors.black),
              ),
            ],
          ),
          ValueListenableBuilder<String>(valueListenable: textController,
              builder: (context, snapShot, _) => Text(snapShot)),
          InputKeyboard(
            addValue: (value) {
              if (textController.value.isEmpty && value == 0) {
                return;
              }
              textController.value = textController.value + value.toString();
            },
            removeValue: () {
              if (textController.value.isNotEmpty) {
                textController.value = textController.value
                    .substring(0, textController.value.length - 1);
              }
            },
            writeCost: sendResult,
            category: widget.category,
          ),
        ],
      ),
    );
  }

  void sendResult() {
    Navigator.of(context).pop(SpendEntity(
        cost: int.tryParse(textController.value) ?? 0, cardId: selectedBill.id));
  }
}

class InputKeyboard extends StatelessWidget {
  const InputKeyboard({required this.addValue,
    required this.removeValue,
    required this.category,
    required this.writeCost,
    Key? key})
      : super(key: key);

  final Function(int) addValue;
  final VoidCallback removeValue;
  final CategoryEntity category;
  final Function writeCost;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery
          .of(context)
          .size
          .height * 0.23,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      child: KeyKeyboard(
                          value: Text('7', textAlign: TextAlign.center),
                          callback: () => addValue.call(7)),
                    ),
                    Container(
                      height: 30,
                      width: 1,
                      color: Theme
                          .of(context)
                          .dividerColor,
                    ),
                    Expanded(
                      child: KeyKeyboard(
                          value: Text('8', textAlign: TextAlign.center),
                          callback: () => addValue.call(8)),
                    ),
                    Container(
                      height: 30,
                      width: 1,
                      color: Theme
                          .of(context)
                          .dividerColor,
                    ),
                    Expanded(
                      child: KeyKeyboard(
                          value: Text('9', textAlign: TextAlign.center),
                          callback: () => addValue.call(9)),
                    ),
                  ],
                ),
                Divider(
                  height: 1,
                ),
                Row(
                  children: [
                    Expanded(
                      child: KeyKeyboard(
                          value: Text('4', textAlign: TextAlign.center),
                          callback: () => addValue.call(4)),
                    ),
                    Container(
                      height: 30,
                      width: 1,
                      color: Theme
                          .of(context)
                          .dividerColor,
                    ),
                    Expanded(
                      child: KeyKeyboard(
                          value: Text('5', textAlign: TextAlign.center),
                          callback: () => addValue.call(5)),
                    ),
                    Container(
                      height: 30,
                      width: 1,
                      color: Theme
                          .of(context)
                          .dividerColor,
                    ),
                    Expanded(
                      child: KeyKeyboard(
                          value: Text('6', textAlign: TextAlign.center),
                          callback: () => addValue.call(6)),
                    ),
                  ],
                ),
                Divider(
                  height: 1,
                ),
                Row(
                  children: [
                    Expanded(
                      child: KeyKeyboard(
                          value: Text('1', textAlign: TextAlign.center),
                          callback: () => addValue.call(1)),
                    ),
                    Container(
                      height: 30,
                      width: 1,
                      color: Theme
                          .of(context)
                          .dividerColor,
                    ),
                    Expanded(
                      child: KeyKeyboard(
                          value: Text('2', textAlign: TextAlign.center),
                          callback: () => addValue.call(2)),
                    ),
                    Container(
                      height: 30,
                      width: 1,
                      color: Theme
                          .of(context)
                          .dividerColor,
                    ),
                    Expanded(
                      child: KeyKeyboard(
                          value: Text('3', textAlign: TextAlign.center),
                          callback: () => addValue.call(3)),
                    ),
                  ],
                ),
                Divider(
                  height: 1,
                ),
                Row(
                  children: [
                    Expanded(
                      child: KeyKeyboard(
                          value: Text(
                            '0',
                            textAlign: TextAlign.center,
                          ),
                          callback: () => addValue.call(0)),
                    ),
                    Container(
                      height: 30,
                      width: 1,
                      color: Theme
                          .of(context)
                          .dividerColor,
                    ),
                  ],
                )
              ],
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              KeyKeyboard(
                  decoration: BoxDecoration(
                    border: Border(
                      left: BorderSide(
                          color: Theme
                              .of(context)
                              .dividerColor,
                          style: BorderStyle.none),
                      bottom: BorderSide(color: Theme
                          .of(context)
                          .dividerColor),
                    ),
                  ),
                  value: Icon(
                    Icons.backspace_outlined,
                    size: 17.5,
                    color: Colors.grey,
                  ),
                  callback: removeValue.call),
              Expanded(
                  child: KeyKeyboard(
                    decoration: BoxDecoration(color: category.color),
                    value: Icon(
                      Icons.check,
                      color: Colors.white,
                    ),
                    callback: () => writeCost.call(),
                  ))
            ],
          ),
        ],
      ),
    );
  }
}

class KeyKeyboard extends StatelessWidget {
  const KeyKeyboard({
    required this.value,
    required this.callback,
    this.decoration,
    Key? key,
  }) : super(key: key);
  final Widget value;
  final VoidCallback callback;
  final BoxDecoration? decoration;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: callback,
      child: Container(
          padding: const EdgeInsets.all(16),
          decoration: decoration,
          child: value),
    );
  }
}
