import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AddBillSheetWidget extends StatefulWidget {
  final String title;
  final List<String> allBillNames;

  AddBillSheetWidget(
    this.title,
    this.allBillNames,
  );

  @override
  State<StatefulWidget> createState() => _AddBillSheetWidgetState();
}

class _AddBillSheetWidgetState extends State<AddBillSheetWidget> {
  final textController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: SafeArea(
          minimum: const EdgeInsets.all(16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: sendResult,
                    child: Text(
                      'Ок',
                      style: TextStyle(color: Colors.black, fontSize: 16),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'Отмена',
                      style: TextStyle(color: Colors.black, fontSize: 16),
                    ),
                  )
                ],
              ),
              Center(
                child: Text(
                  widget.title,
                  style: TextStyle(color: Colors.black),
                ),
              ),
              Form(
                key: _formKey,
                child: TextFormField(
                  validator: (value) {
                    if (widget.allBillNames.contains(value)) {
                      return 'Данное название счета уже существует';
                    }
                    return null;
                  },
                  controller: textController,
                  keyboardType: TextInputType.name,
                  style: TextStyle(color: Colors.black),
                  inputFormatters: [
                    FilteringTextInputFormatter.singleLineFormatter
                  ],
                  onEditingComplete: sendResult,
                ),
              ),
            ],
          )),
    );
  }

  void sendResult() {
    if (_formKey.currentState!.validate()) {
      Navigator.of(context).pop(textController.text);
    }
  }
}
