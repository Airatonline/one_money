import 'package:flutter/material.dart';

class CategoryIcon extends StatelessWidget {
  final Color categoryColor;
  final IconData categoryIcon;

  const CategoryIcon({required this.categoryColor, required this.categoryIcon});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(100),
      child: Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(color: categoryColor),
        child: Icon(categoryIcon),
      ),
    );
  }
}
