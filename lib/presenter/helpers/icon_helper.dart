import 'package:flutter/cupertino.dart';

class IconHelper {
  static IconData getIconData(int icon) {
    return IconData(icon, fontFamily: 'MaterialIcons');
  }
}
