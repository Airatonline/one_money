import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:one_money/domain/entity/category_entity.dart';
import 'package:one_money/domain/entity/spend_entity.dart';
import 'package:one_money/presenter/cards/cards_cubit.dart';
import 'package:one_money/presenter/widgets/category_icon.dart';
import 'package:one_money/presenter/widgets/input_value_sheet_widget.dart';

import 'category_cubit.dart';

class CategoryScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CategoryCubit, CategoryState>(
      builder: (context, state) => Column(
        children: [
          SizedBox(
            height: 140,
            child: AppBar(
              centerTitle: true,
              title: _buildInvoiceTitle(context),
              // actions: [
              //   IconButton(
              //     onPressed: () {},
              //     icon: Icon(Icons.edit),
              //   )
              // ],
              bottom: _buildBottomTabBar(context),
            ),
          ),
          Expanded(
            child: GridView.count(
              crossAxisCount: 4,
              crossAxisSpacing: 4,
              children: state.categoryList.entries
                  .map(
                    (e) => InkWell(
                      onTap: () async {
                        final bills = await CardsCubit.of(context)
                            .repository
                            .getAllMoney();
                        final value = await showModalBottomSheet<SpendEntity>(
                            isScrollControlled: true,
                            context: context,
                            builder: (_) =>
                                InputValueSheetWidget(e.value, bills));
                        if (value != null) {
                          CategoryCubit.of(context).addCost(e.key, value.cost, value.cardId);
                          await CardsCubit.of(context).init();
                        }
                      },
                      child: _buildCategoryElement(e.value),
                    ),
                  )
                  .toList(),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildInvoiceTitle(BuildContext context) {
    final cubit = CategoryCubit.of(context);
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(
          'Все счета',
          style: Theme.of(context)
              .appBarTheme
              .titleTextStyle!
              .copyWith(fontSize: 12),
        ),
        SizedBox(
          height: 4,
        ),
        Text(
          '${cubit.state.allCost}₽',
          style: Theme.of(context)
              .appBarTheme
              .titleTextStyle!
              .copyWith(fontSize: 20),
        ),
      ],
    );
  }

  PreferredSize _buildBottomTabBar(BuildContext context) {
    final cubit = CategoryCubit.of(context);
    return PreferredSize(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            IconButton(
                onPressed: () {
                  cubit.changeMonth(DateTime(cubit.state.startTime.year,
                      cubit.state.startTime.month - 1));
                },
                icon: Icon(Icons.chevron_left)),
            Text(
              '${DateFormat('MMMM').format(cubit.state.startTime)}',
              style: Theme.of(context)
                  .appBarTheme
                  .titleTextStyle!
                  .copyWith(fontSize: 14),
            ),
            IconButton(
                onPressed: () {
                  cubit.changeMonth(DateTime(cubit.state.startTime.year,
                      cubit.state.startTime.month + 1));
                },
                icon: Icon(Icons.chevron_right)),
          ],
        ),
        preferredSize: Size(double.infinity, 40));
  }

  Widget _buildCategoryElement(
    CategoryEntity category,
  ) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(category.name),
        CategoryIcon(
            categoryColor: category.color, categoryIcon: category.icon),
        Text(
          '${category.cost}₽',
          style: TextStyle(color: category.color),
        )
      ],
    );
  }
}
