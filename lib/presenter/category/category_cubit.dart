import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:one_money/data/models/spend_element_model.dart';
import 'package:one_money/data/repository/interface/repository.dart';
import 'package:one_money/domain/entity/category_entity.dart';
import 'package:one_money/presenter/helpers/icon_helper.dart';

class CategoryCubit extends Cubit<CategoryState> {
  CategoryCubit()
      : super(
          CategoryState(
            categoryList: {},
            startTime: DateTime(DateTime.now().year, DateTime.now().month),
            endTime: DateTime(DateTime.now().year, DateTime.now().month + 1),
          ),
        );

  final repository = Injector().get<Repository>();

  static CategoryCubit of(BuildContext context) =>
      BlocProvider.of<CategoryCubit>(context);

  Future<void> init() async {
    final categoryList = await repository.getCategories();
    //TODO: refactor IT!
    final spendElements =
        await repository.getSpendGoods(state.startTime, state.endTime);
    final categoryMap = <int, CategoryEntity>{};
    categoryList.forEach((element) {
      int cost = 0;
      spendElements
          .where((spend) => element.id == spend.categoryId)
          .forEach((categorySpend) {
        cost += categorySpend.cost;
      });
      categoryMap[element.id] = CategoryEntity(
          name: element.name,
          icon: IconHelper.getIconData(element.icon),
          color: Color(element.color),
          cost: cost);
    });
    emit(
      state.copyWith(
        categoryList: categoryMap,
        allCost: await repository.getBalance(),
      ),
    );
  }

  void addCost(int categoryId, int addPrice, int cardId) {
    repository.saveSpend(SpendElementModel(
        cost: addPrice,
        time: DateTime.now(),
        categoryId: categoryId,
        cardId: cardId));
    init();
  }

  void changeMonth(DateTime newStartTime) {
    emit(
      state.copyWith(
        startTime: newStartTime,
        endTime: DateTime(newStartTime.year, newStartTime.month + 1),
      ),
    );
    init();
  }
}

class CategoryState {
  final Map<int, CategoryEntity> categoryList;
  final DateTime startTime;
  final DateTime endTime;
  final double allCost;

  const CategoryState(
      {required this.categoryList,
      required this.startTime,
      required this.endTime,
      this.allCost = 0});

  CategoryState copyWith(
      {Map<int, CategoryEntity>? categoryList,
      DateTime? startTime,
      DateTime? endTime,
      double? allCost}) {
    return CategoryState(
        categoryList: categoryList ?? this.categoryList,
        startTime: startTime ?? this.startTime,
        endTime: endTime ?? this.endTime,
        allCost: allCost ?? this.allCost);
  }
}
