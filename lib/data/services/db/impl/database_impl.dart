import 'package:hive/hive.dart';
import 'package:one_money/data/models/category_model.dart';
import 'package:one_money/data/models/money_model.dart';
import 'package:one_money/data/models/spend_element_model.dart';
import 'package:one_money/data/services/db/inteface/database.dart';

class DatabaseImpl extends Database {
  final Box<SpendElementModel> spendBox;
  final Box<CategoryModel> categoryBox;
  final Box<MoneyModel> moneyBox;

  DatabaseImpl(this.spendBox, this.categoryBox, this.moneyBox);

  @override
  List<SpendElementModel>? getSpendListBetweenKeys(
      DateTime startTime, DateTime endTime) {
    final spends = spendBox.values;
    return spends
        .where(
          (element) =>
              element.time.isAfter(startTime) && element.time.isBefore(endTime),
        )
        .toList();
  }

  @override
  List<SpendElementModel> getAllSpends() {
    return spendBox.values.toList();
  }

  @override
  Future<void> saveSpend(SpendElementModel model) {
    return spendBox.add(model);
  }

  @override
  Future<void> addCategory(CategoryModel category) {
    return categoryBox.add(category);
  }

  @override
  List<CategoryModel> getCategories() {
    return categoryBox.values.toList();
  }

  @override
  Future<void> removeCategory(CategoryModel category) {
    final index = categoryBox.values
        .toList()
        .indexWhere((element) => element.name == category.name);
    return categoryBox.deleteAt(index);
  }

  @override
  List<MoneyModel> getMoney() {
    final moneys = moneyBox.values;
    return moneys.toList();
  }

  @override
  Future<void> saveMoney(MoneyModel money) {
    if (moneyBox.isEmpty) {
      return moneyBox.add(
        MoneyModel(0, money.name, money.value),
      );
    }

    final index = moneyBox.values
        .toList()
        .indexWhere((element) => element.id == money.id);
    return moneyBox.putAt(index,
        money.copyWith(value: money.value + moneyBox.getAt(index)!.value));
  }

  @override
  Future<void> addBill(String name) {
    return moneyBox.add(MoneyModel(moneyBox.length, name, 0));
  }
}
