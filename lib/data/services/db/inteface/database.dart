import 'package:one_money/data/models/category_model.dart';
import 'package:one_money/data/models/money_model.dart';
import 'package:one_money/data/models/spend_element_model.dart';

abstract class Database {
  List<CategoryModel> getCategories();

  Future<void> addCategory(CategoryModel category);

  Future<void> removeCategory(CategoryModel category);

  List<SpendElementModel>? getSpendListBetweenKeys(
      DateTime startTime, DateTime endTime);

  List<SpendElementModel> getAllSpends();

  Future<void> saveSpend(SpendElementModel model);

  Future<void> saveMoney(MoneyModel money);

  List<MoneyModel> getMoney();

  Future<void> addBill(String name);
}
