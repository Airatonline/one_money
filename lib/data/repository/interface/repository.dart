import 'package:one_money/data/models/category_model.dart';
import 'package:one_money/data/models/money_model.dart';
import 'package:one_money/data/models/spend_element_model.dart';

abstract class Repository {
  Future<List<SpendElementModel>> getSpendGoods(
      DateTime startTime, DateTime endTime);

  Future<void> saveSpend(SpendElementModel spend);

  Future<List<CategoryModel>> getCategories();

  Future<void> addCategory(CategoryModel model);

  Future<void> saveMoney(MoneyModel money);

  Future<List<MoneyModel>> getAllMoney();

  Future<void> addBill(String value);

  Future<double> getBalance();
}
