import 'package:flutter/material.dart';
import 'package:one_money/data/models/category_model.dart';
import 'package:one_money/data/models/money_model.dart';
import 'package:one_money/data/models/spend_element_model.dart';
import 'package:one_money/data/repository/interface/repository.dart';
import 'package:one_money/data/services/db/inteface/database.dart';

class RepositoryImpl extends Repository {
  final Database db;

  RepositoryImpl(this.db);

  @override
  Future<List<SpendElementModel>> getSpendGoods(
      DateTime startTime, DateTime endTime) async {
    final spendList = db.getSpendListBetweenKeys(startTime, endTime);
    if (spendList == null || spendList.isEmpty) {
      return [];
    }
    return spendList;
  }

  List<SpendElementModel> _getAllSpends() {
    return db.getAllSpends();
  }

  @override
  Future<void> saveSpend(SpendElementModel spend) {
    return db.saveSpend(spend);
  }

  @override
  Future<void> addCategory(CategoryModel model) {
    return db.addCategory(model);
  }

  @override
  Future<List<CategoryModel>> getCategories() async {
    final categories = db.getCategories();
    if (categories.isEmpty) {
      db.addCategory(
          CategoryModel(0, 'Кафе', Icons.tapas.codePoint, 0xFF2196F3));
      db.addCategory(CategoryModel(
          1, 'Досуг', Icons.wine_bar_sharp.codePoint, 0xFFFF5722));
      db.addCategory(CategoryModel(
          2, 'Транспорт', Icons.directions_bus.codePoint, 0xFFFF9800));
      db.addCategory(CategoryModel(
          3, 'Продукты', Icons.shopping_bag_outlined.codePoint, 0xFF00BCD4));
      db.addCategory(CategoryModel(
          4, 'Подарки', Icons.card_giftcard.codePoint, 0xFFF44336));
      db.addCategory(
          CategoryModel(5, 'Здоровье', Icons.healing.codePoint, 0xFF4CAF50));
      db.addCategory(
          // не пугайся, это самое большое int значение, чтобы категория точно была последней)
          CategoryModel(0x7fffffffffffffff, 'Другое', Icons.healing.codePoint,
              0xFF7979FF));
      return db.getCategories();
    }
    categories.sort((a, b) => a.id - b.id);
    return categories;
  }

  @override
  Future<List<MoneyModel>> getAllMoney() async {
    var moneys = db.getMoney();
    if (moneys.isEmpty) {
      await addBill('Карты');
      moneys = db.getMoney();
    }
    final spends = _getAllSpends();
    final moneysWithSpends = <MoneyModel>[];
    moneys.forEach((element) {
      final spendsListBill = spends.where((e) => e.cardId == element.id);
      if (spendsListBill.isEmpty) {
        moneysWithSpends
            .add(MoneyModel(element.id, element.name, element.value));
      } else {
        final allSpendsToBill = spendsListBill
            .map((e) => e.cost)
            .reduce((value, element) => value + element);
        moneysWithSpends.add(MoneyModel(
            element.id, element.name, element.value - allSpendsToBill));
      }
    });
    return moneysWithSpends;
  }

  @override
  Future<void> saveMoney(MoneyModel money) => db.saveMoney(money);

  @override
  Future<void> addBill(String value) => db.addBill(value);

  @override
  Future<double> getBalance() async {
    final debit = (await getAllMoney())
        .map((e) => e.value)
        .reduce((value, element) => value + element);
    return debit;
  }
}
