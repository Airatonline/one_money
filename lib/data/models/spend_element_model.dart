import 'package:hive/hive.dart';

part 'spend_element_model.g.dart';

@HiveType(typeId: 0)
class SpendElementModel extends HiveObject {
  @HiveField(0)
  final int cost;
  @HiveField(1)
  final DateTime time;
  @HiveField(2)
  final int categoryId;
  @HiveField(3)
  final int cardId;

  SpendElementModel(
      {required this.cost,
      required this.time,
      required this.categoryId,
      required this.cardId});
}
