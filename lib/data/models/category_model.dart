import 'package:hive/hive.dart';

part 'category_model.g.dart';

@HiveType(typeId: 1)
class CategoryModel extends HiveObject {
  @HiveField(0)
  final int id;
  @HiveField(1)
  final String name;
  @HiveField(2)
  final int icon;
  @HiveField(3)
  final int color;

  CategoryModel(this.id, this.name, this.icon, this.color);
}
