import 'package:hive/hive.dart';

part 'money_model.g.dart';

@HiveType(typeId: 2)
class MoneyModel extends HiveObject {
  @HiveField(0)
  final int id;
  @HiveField(1)
  final String name;
  @HiveField(2)
  final double value;

  MoneyModel(this.id, this.name, this.value);

  MoneyModel copyWith({String? name, double? value}) {
    return MoneyModel(this.id, name ?? this.name, value ?? this.value);
  }
}
