// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'spend_element_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class SpendElementModelAdapter extends TypeAdapter<SpendElementModel> {
  @override
  final int typeId = 0;

  @override
  SpendElementModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    if (fields[3] == null) {
      fields[3] = 0;
    }
    return SpendElementModel(
      cost: fields[0] as int,
      time: fields[1] as DateTime,
      categoryId: fields[2] as int,
      cardId: fields[3] as int,
    );
  }

  @override
  void write(BinaryWriter writer, SpendElementModel obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.cost)
      ..writeByte(1)
      ..write(obj.time)
      ..writeByte(2)
      ..write(obj.categoryId)
      ..writeByte(3)
      ..write(obj.cardId);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SpendElementModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
