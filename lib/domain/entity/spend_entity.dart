class SpendEntity {
  final int cost;
  final int cardId;

  const SpendEntity({required this.cost, required this.cardId});
}
