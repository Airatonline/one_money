import 'package:flutter/cupertino.dart';

class CategoryEntity {
  final String name;
  final IconData icon;
  final Color color;
  final int cost;

  const CategoryEntity(
      {required this.name,
      required this.icon,
      required this.color,
      required this.cost});

  CategoryEntity copyWith(
          {String? name, IconData? icon, Color? color, int? cost}) =>
      CategoryEntity(
          name: name ?? this.name,
          icon: icon ?? this.icon,
          color: color ?? this.color,
          cost: cost ?? this.cost);
}
