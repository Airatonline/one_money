import 'package:flutter/material.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:one_money/di/di.dart';

import 'home_screen.dart';

void main()  async{
  WidgetsFlutterBinding.ensureInitialized();
  await DI().initialize(Injector());
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final primarySwatch = Colors.blue;
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: primarySwatch,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        iconTheme: IconTheme.of(context).copyWith(
          color: Colors.white,
        ),
        appBarTheme: Theme.of(context)
            .appBarTheme
            .copyWith(titleTextStyle: TextStyle(color: Colors.white)),
        bannerTheme:
            MaterialBannerThemeData(backgroundColor: primarySwatch.shade100),
        textTheme: Theme.of(context)
            .textTheme
            .copyWith(headline1: TextStyle(color: Colors.black, fontSize: 18)),
      ),
      home: FutureBuilder(
        future: Future.value(null),
        builder: (context, snapshot) =>
            snapshot.connectionState == ConnectionState.done
                ? HomeScreen()
                : Scaffold(
                    body: Center(
                      child: CircularProgressIndicator(),
                    ),
                  ),
      ),
    );
  }
}
